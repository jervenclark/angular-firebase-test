import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database-deprecated';

@Component({
  selector: 'app-book-info',
  templateUrl: './book-info.component.html',
  styleUrls: ['./book-info.component.scss'],
  providers: [
    AngularFireDatabase
  ]
})
export class BookInfoComponent implements OnInit {

  constructor(
    private afDB: AngularFireDatabase
  ) { }

  bookObservable;

  ngOnInit() {
    this.bookObservable = this.getBook('atlas-shrugged');
  }

  getBook(bookId: string): FirebaseObjectObservable<any> {
    const path = `/books/${bookId}`;
    return this.afDB.object(path);
  }

}
