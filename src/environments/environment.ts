// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCvh4IwyjPafVKlgR4PoaXdZ5MEwxATiMk",
    authDomain: "messaging-75d8d.firebaseapp.com",
    databaseURL: "https://messaging-75d8d.firebaseio.com",
    projectId: "messaging-75d8d",
    storageBucket: "messaging-75d8d.appspot.com",
    messagingSenderId: "404083229974"
  }
};
